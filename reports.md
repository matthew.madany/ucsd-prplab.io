---
layout: page
title: Workshop Reports
short: reports
siteimage: reports.png
---

| Date | Workshop
|------|---------
| 2019.09.17 | [1st GRP Workshop][grp1]
| 2018.09.20 | [2nd NRP Workshop][nrp2]
| 2018.09.06 | [DIBBs18 Workshop - Final Draft][dibbs18]
| 2018.03.20 | [Science Engagement: Best Practices & Scalability in the 21st Century by Trisha Gorman][nrpprp2018]
| 2017.10.23 | [NRP Workshop][nrp2017]
| 2017.08.23 | [Big Data Workshop][bdwr17]
| 2017.02.21 | [PRP Workshop][prp2017]
| 2015.12.16 | [Pacific Research Platform 2015 Workshop][prp2015]

[nrp2017]: /images/reports/NRP_Workshop_Report_FINAL-23-Oct-17.pdf
[bdwr17]: /images/reports/BigDataWorkshop2017_Report_FINAL_082417.pdf
[prp2017]: /images/reports/PRP_V2_Workshop_Report-2-21-17-final.pdf
[prp2015]: /images/reports/Final_Workshop_Report__Smarr_ACI-1540112_12-16-15.pdf
[dibbs18]: /images/reports/DIBBs18_Final_Report.pdf
[nrpprp2018]: /images/reports/Science_Engagement--PRP-NRP_Report-FINAL_3-20-2018a.pdf
[nrp2]: /images/reports/2NRP_Workshop_Report_final-small-9-20-18.pdf
[grp1]: /images/reports/GRP_Workshop_Report-012920-DISTRIBUTIONv2.pdf
