---
layout: collection-namespace
title: Namespaces
short: namespaces
siteimage: nautilus.png
date: 2018-12-10
categories:
- "nautilus"
tags: []
---
**Namespaces** are virtual clusters supported by Kubernetes and they
provide  a way to divide cluster resources among multiple users.
PRP Nautilus cluster supports multiple namespaces for different teams of users and
projects.  A few examples are listed in a gallery below. 
**To find more info about each namespace follow the links inside the cards.**
