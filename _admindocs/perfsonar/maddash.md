---
layout: collection-admindocs
title: MaDDash 
date: 2019-04-04
short: admindocs
categories: user
order: 2
---

<div class="border">
<strong>MaDDash Links</strong>
</div>
[PRP][maddash1]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[NRP][maddash2]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[CENIC][maddash3]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[Pacific Wave][maddash4]


<div class="border">
<strong>Setting up perfSONAR</strong>
</div>

Links for the updated FIONA perfSONAR and GridFTP materials from a 2-day
workshop held at Calit2, UCSD, March 2019.   

| -- | -- |
|[Workshop agenda][ws-agenda] | [psConfig for MaDDash] [ws-psconfig] |
|[Workshop materials (gitlab repo)][ws-materials] | [Exercise: setup perfsonar-centralmanagement bundle][ws-labs]|

[maddash1]: https://perfsonar.nautilus.optiputer.net/maddash-webui/
[maddash2]: https://perfsonar.nrp-nautilus.io/maddash-webui/
[maddash3]: https://ps-dashboard.cenic.net/maddash-webui/
[maddash4]: https://ps-dashboard.pacificwave.net
[ws-agenda]: https://ucsd-prp.gitlab.io/lajolla2019-agenda/
[ws-materials]:  https://gitlab.com/ucsd-prp/presentations/lajolla-2019
[ws-psconfig]: https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/maddash--ma/PRP-FIONA-Workshop-LaJolla-MaDDash-jhess-final.pdf
[ws-labs]: https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/maddash--ma/lajolla-4.1--perfsonar-centralmanagement.md
