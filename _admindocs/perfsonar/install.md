---
layout: collection-admindocs
title: Installation 
date: 2019-04-10
short: admindocs
categories: user
order: 1
---

<div class="border">
<strong>perfSONAR installation options</strong>
</div>

The **perfsonar-testpoint** bundle provides the minimal package elements required for the pS node to participate in mesh-orchestrated 
tests, and register results to a (central) measurement archive. Other acceptable bundles include **perfsonar-core**, and **perfsonar-toolkit**.
See official perfSONAR [documentation][install]

[install]: http://docs.perfsonar.net/install_options.html


