---
layout: collection-admindocs
title: Participating
date: 2020-11-18
short: admindocs
categories: admin
order: 0
---

This section contains pages that provide information on how to participate in Nautilus cluster by joining your hardware. 

Use menu on the left for topics in this section.<br>
Use tabs in the top navigation bar to switch to another section.
