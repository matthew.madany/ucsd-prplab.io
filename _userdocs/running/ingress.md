---
layout: collection-userdocs
title: Exposing HTTP
date: 2020-05-01
short: userdocs
categories: user
order: 65
---

While pods are not accessible from outside the cluster, you can expose the http services provided by pods by using the [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) controllers. We have [Traefik v2](https://containo.us/traefik/) installed for this purpose.

To expose a port in your pod, you first need to create a service for it.

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    k8s-app: test-svc
  name: test-svc
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    k8s-app: test-http
  type: ClusterIP
```

Where `spec.selector.<label>` should match the label of the target pod, and `targetPort` should match the port you want to expose.

After that you can create the Ingress object:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: traefik
    traefik.ingress.kubernetes.io/router.tls: ""
  name: test-ingress
spec:
  rules:
  - host: test-service.nautilus.optiputer.net
    http:
      paths:
      - backend:
          serviceName: test-svc
          servicePort: 80
        path: /
```

You can choose the host subdomain to be whatever you want (`<whatever>.nautilus.optiputer.net`). This is enough to have your pod served under `test-service.nautilus.optiputer.net`.

#### Using my own domain name

If you need to use your own domain, you would have to also provide a valid certificate in a secret in your namespace:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-own-hostname-tls
type: kubernetes.io/tls
data:
  ca.crt: <optional base64-encoded ca>
  tls.crt: <base64-encoded crt>
  tls.key: <base64-encoded key>
```

and add a section to the Ingress:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: traefik
    traefik.ingress.kubernetes.io/router.tls: ""
  name: test-ingress
spec:
  rules:
  - host: my-own-hostname.com
    http:
      paths:
      - backend:
          serviceName: test-svc
          servicePort: 80
        path: /
  tls:
  - hosts:
    - my-own-hostname.com
    secretName: my-own-hostname-tls
```

Create the CNAME DNS record for your domain pointing to `nautilus.optiputer.net`.

#### Auto renewing the certificate

Traefik v2 supports auto retrieving and updating [ACME Let's Encrypt](https://letsencrypt.org/docs/client-options/) certificates using the [CertManager](https://cert-manager.io). [Contact us](/userdocs/start/contact) if you need to support this for your domain. You'll need your DNS located in one of [CertManager supported DNS providers](https://cert-manager.io/docs/configuration/acme/dns01/#supported-dns01-providers).