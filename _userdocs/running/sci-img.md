---
layout: collection-userdocs
title: Scientific software
date: 2020-07-11
short: userdocs
categories: user
order: 12
---

There is a set of container images that you can use in your computational workflows without building your own. The images are based on [Docker Stack](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html) project with NVIDIA CUDA libraries added, so that you could use those with GPUs.

All images are available in [JupyterLab](https://jupyterhub.nautilus.optiputer.net), and also you can refer the images directly in your [Pod definition](/userdocs/running/jupyter/) by copying the corresponding address from the [GitLab registry](https://gitlab.nautilus.optiputer.net/prp/jupyter-stack/container_registry).

The PRP image is based on Tensorflow one with addition of:

```
    astropy
    bowtie
    fastai
    keras
    nbgitpuller
    opencv-python
    psycopg2-binary
    tensorflow-probability
    torch
    torchvision
    visualdl
    git+https://github.com/veeresht/CommPy.git
```

We can add more libraries to this image on request in [RocketChat](https://rocket.nautilus.optiputer.net).

The Desktop image has X Window system installed and you can launch the GUI interface in Jupyter with this image. It's based on Minimal stack one.

If you're using these images in your jobs, you can [speed up the image download time by using Kraken](/userdocs/running/fast-img-download).
