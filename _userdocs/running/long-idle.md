---
layout: collection-userdocs
title: Long idle pods 
date: 2020-03-09
short: userdocs
categories: user
order: 17
---

In case you need to have an idle pod in the cluster, that might ocassionally do some computations, you have to run it as a [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/). Deployments in Nautilus are limited to 2 weeks (unless the namespace is added to exceptions list and runs a permanent service). This ensures your pod will not run in the cluster forever when you don't need it and move on to other projects.

Please don't run such pods as Jobs, since those are not purged by the cleaing daemon and will stay in the cluster forever if you forget to remove those.

Such a deployment **can not request a GPU**. You can use the 

```yaml
  command:
    - sleep
    - "100000000"
```

as the command if you just want a pure shell, and `busybox`, `centos`, `ubuntu` or any other general image you like.

Follow the [guide for creating deployments](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) and add the minimal requests to it and limits that make sense, for example:

```yaml
    resources:
      limits:
        cpu: "1"
        memory: 10Gi
      requests:
        cpu: "10m"
        memory: 100Mi
```

Example of running an nginx deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    k8s-app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: nginx
  template:
    metadata:
      labels:
        k8s-app: nginx
    spec:
      containers:
      - image: nginx
        name: nginx-pod
        resources:
          limits:
            cpu: 1
            memory: 4Gi
          requests:
            cpu: 100m
            memory: 500Mi
```
