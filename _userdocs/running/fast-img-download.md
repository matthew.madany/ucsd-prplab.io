---
layout: collection-userdocs
title: Faster images download
date: 2020-07-11
short: userdocs
categories: user
order: 15
---

The images registry is normally a single web app which is not scalable, and several gigabyte images can take a long time to be downloaded. In our cluster we're running the [UBER Kraken](https://github.com/uber/kraken) - a peer-to-peer layer that caches images layers on cluster nodes and provides significantly improved download speed.

Currently it's set up to serve the images from the following registries:

| Registry                                      | Prefix      |
|-----------------------------------------------|-------------|
| gitlab-registry.nautilus.optiputer.net/prp    | prp         |
| gitlab-registry.nautilus.optiputer.net/ar-noc | ar-noc      |
| gitlab-registry.nautilus.optiputer.net/madany | madany      |
| docker hub (hub.docker.io) official images    | library     |
| docker hub (hub.docker.io) user images        |             |


If you need some other image cached, let us know in [Matrix](/userdocs/start/contact/).

The way to use kraken is to specify the image as `localhost:30081/prefix/<your_image>`, for example `gitlab-registry.nautilus.optiputer.net/prp/jupyter:latest` becomes `localhost:30081/prp/jupyter:latest`. For user-uploaded docker hub images use the user name as prefix. Nodes will download and cache your image, and next time the download to another node will be much faster.

**There is now automatic replacement of supported image URLs to use kraken for all pods. If you want your namesapce excluded for some reason, let us know in [Matrix](/userdocs/start/contact/).**
