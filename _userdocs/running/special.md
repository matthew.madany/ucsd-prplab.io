---
layout: collection-userdocs
title: Special use
date: 2019-04-08
short: userdocs
categories: user
order: 90
---

Our cluster combines various hardware resources from multiple universities and other organizations. By default you can only use the **production** nodes (see the [resources page](https://nautilus.optiputer.net/resources) of the portal).

You can only tolerate nodes mentioned on this page. All other tolerations can be used only after explicit permission from admins.

#### CHASE-CI tolerations

If your project is related to one of these persons:

|--|--|--|--|--|
| Ken Kreutz-Delgado| Tajana Simunic Rosing| Amit K. Roy Chowdhury| Walid Najjar| 
| Nikil Dutt| Trevor Darrell| Lise Getoor| Anshul Kundaje| 
| Gary Cottrell| Frank Wuerthwein| Hao Su| Dinesh Bharadia| 
| YangQuan Chen| Jeff Krichmar| Charless Fowlkes| Padhraic Smyth| 
| James Demmel| Yisong Yue| Shawfeng Dong| Rajesh Gupta| 
| Todd Hylton| Falko Kuester| Jurgen Schulze| Arun Kumar| 
| Ron Dror| Ravi Ramamoorthi| John Sheppard| Nuno Vasconcelos| 
| Ramakrishna Akella| Manmohan Chandraker| Baris Aksanli| Dimitris Achlioptas| 
| Ilkay Altintas| Brad Smith| Christopher Paolini| Jerry Sheehan| 


, which means you've specified the person as a PI in the namespace description, it will also be assigned to nodes tainted as Chase-CI ("chaseci"). **This will give you more GPU nodes and shorter wait time.**


#### XILINX FPGA nodes

Current installation of Xilinx FPGAs require running nodes with an older kernel. Those nodes require the `nautilus.io/oldkernel` toleration:

```
spec:
  tolerations:
  - key: "nautilus.io/oldkernel"
    operator: "Exists"
    effect: "NoSchedule"
```

#### Other tolerations

Some nodes in the cluster don't have access to public Internet, and can only access educational network. They still can pull images from Docker Hub using a proxy.

If your workload is not using the public Internet resources, you might tolerate the `nautilus.io/science-dmz` and get access to additional nodes.
