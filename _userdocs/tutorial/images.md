---
layout: collection-userdocs
title: Images
date: 2020-05-01
short: userdocs
categories: user
order: 30
---

## Finding container images and building your own

Multiple pre-built publicly available continer images already exist on [Docker Hub](https://hub.docker.com). If there's one that fits your needs, you can use one directly from there.

If there's an image that you want to use, but also need a couple more libraries added to it, you can extend the image by building your own based on remote one.

First, create an account in our [GitLab](https://gitlab.nautilus.optiputer.net) instance to simplify building and storing the containers.

## Extending an existing image

Find the image you need. Let's say we want to extend the [python:3](https://hub.docker.com/_/python) image by adding a jupyter package to it, and also installing the vim text editor.

Follow the [GitLab development guide](/userdocs/development/gitlab/) (make it public to simplify accessing your image), and also add this `Dockerfile` to the project before commiting your changes:

```
FROM python:3

RUN pip install jupyter && \
  apt-get update && \
  apt-get install -y vim
```

Use the [basic section](/userdocs/tutorial/basic) to run the pod with your new image.

Exec into the pod and check jupyter and vim are installed:

```bash
root@test-pod:/# vim -h | head -n 1
VIM - Vi IMproved 8.1 (2018 May 18, compiled Jun 15 2019 16:41:15)
root@test-pod:/# jupyter
usage: jupyter [-h] [--version] [--config-dir] [--data-dir] [--runtime-dir] [--paths] [--json] [subcommand]
jupyter: error: one of the arguments --version subcommand --config-dir --data-dir --runtime-dir --paths is required
```

## Changing an existing image

Sometimes you see an image which almost work for you, but you'd like to change versions of the software of the software installed in it. Some images provide the source of their Dockerfile, which is usually well written and you can use it as is, but modify the versions.

For example, we take the [mltshp/mltshp-web](http://hub.docker.com/r/mltshp/mltshp-web) image, and go to it's [Dockerfile](https://hub.docker.com/r/mltshp/mltshp-web/dockerfile). Then we copy it's contents and use the above steps to build our own image with needed changes.

## The end

Please make sure you deleted the project in `Settings -> General -> Advanced` when done.