---
layout: collection-userdocs
title: Container build
date: 2019-02-08
short: userdocs
categories: user
order: 1
---

We use our own installation of [GitLab][1] for [Source Code Management][2], [Continuous Integration automation][3], 
docker containers registry and other development lifecycle tasks. It fully uses Nautilus Cluster resources, which provides our users unlimited storage and fast builds.
All data from our GitLab except container images are backed up nightly to Google S3. 

##### Step 1: Create a Git repo
1. To use our GItLab installation, register at [https://gitlab.nautilus.optiputer.net][4]
1. Use GitLab for storing your code like any git repository. Here's [GitLab basics guide][5].
1. [create a new project][project] in your GitLab account 

##### Step 2: Use Containers Registry
What makes GitLab especially useful for kubernetes cluster in integration with
Docker Registry. You can store your containers directly in our cluster and
avoid slow downloads from [DockerHub][dockerhub] (although you're still free to do that as well). To use containers registry: 
1. go to your project's general settings and enable the Container Registry. See this instructions [Container Registry][registry]. 
  (It's enabled by default for all projects in our GitLab)

##### Step 3: Continuous Integration automation
To fully unleash the GitLab powers, introduce yourself to [Continuous Integration automation][3] 

1. create the `.gitlab-ci.yml` file in your project, see [Quick start guide][quickstart]. The runners are already configured.  
   There's a list of CI templates available for most common languages.
1. If you need to build your Dockerfile and create a container from it, adjust this `.gitlab-ci.yml` template:

   ```yaml
   image: gcr.io/kaniko-project/executor:debug
   
   stages:
     - build-and-push
   
   build-and-push-job:
     stage: build-and-push
     script:
       - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
       - /kaniko/executor --cache=true --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:${CI_COMMIT_SHA:0:8} --destination $CI_REGISTRY_IMAGE:latest
   ``` 

   [More advanced example][portal_example]

1. Go to `CI / CD -> Jobs` tab to see in amazement your job running and image being uploaded to your registry. 
1. From the `Packages -> Containers Registry` tab get the URL of your image to be included in your pod definition:

   ```yaml
   spec:
     containers:
     - name: my-container
       image: gitlab-registry.nautilus.optiputer.net/<your_group>/<your_project>:<optional_tag>
   ``` 

##### Build better containers

Make yourself familiar with [Docker containers best practices](https://www.docker.com/blog/intro-guide-to-dockerfile-best-practices)

Use [multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build) when necessary


[1]: https://about.gitlab.com/what-is-gitlab/
[2]: https://about.gitlab.com/product/source-code-management/ 
[3]: https://about.gitlab.com/product/continuous-integration/ 
[4]: https://gitlab.nautilus.optiputer.net
[5]: https://docs.gitlab.com/ee/gitlab-basics/
[dockerhub]: https://www.docker.com 
[project]: https://docs.gitlab.com/ee/gitlab-basics/create-project.html 
[registry]: https://docs.gitlab.com/ee/user/project/container_registry.html
[quickstart]: https://docs.gitlab.com/ce/ci/quick_start/
[portal_example]: https://gitlab.nautilus.optiputer.net/prp/k8s_portal/blob/master/.gitlab-ci.yml
