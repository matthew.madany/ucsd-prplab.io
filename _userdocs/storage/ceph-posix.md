---
layout: collection-userdocs
title: Posix
date: 2019-01-10
short: userdocs
categories: user
order: 2
---

In Nautilus cluster we use Ceph distributed storage system orchestrated by Rook. It provides several ways to access your data.

In addition to that we allow access to the BeeGFS filesystem provided by SDSU.

**We now have storage located in several geographic regions. Make sure you use the [right compute nodes](#using-the-right-region-for-your-pod) to ensure the optimal speed accessing it!**

#### Posix volumes
{: id="posix"}

Persistent data in kubernetes comes in a form of [Persistent Volumes (PV)](https://kubernetes.io/docs/concepts/storage/persistent-volumes/), which can only be seen by cluster admins. To request a PV, you have to create a [PersistentVolumeClaim (PVC)](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims) of a supported [StorageClass](https://kubernetes.io/docs/concepts/storage/storage-classes/) in your namespace, which will allocate storage for you.

###### Currently available storageClasses:

<table>
  <thead>
    <tr class="header">
      <th>StorageClass</th>
      <th>Filesystem Type</th>
      <th>Region</th>
      <th>AccessModes</th>
      <th><span style="color:red">Restrictions</span></th>
      <th>Storage Type</th>
      <th>Size</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td markdown="span">rook-cephfs</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">2.1 PB</td>
    </tr>
    <tr>
      <td markdown="span">rook-cephfs-east</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">US East</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span">Mixed</td>
      <td markdown="span">500 TB</td>
    </tr>
    <tr>
      <td markdown="span">beegfs</td>
      <td markdown="span">BeeGFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span"></td>
      <td markdown="span">2PB</td>
    </tr>
    <tr>
      <td markdown="span">rook-cephfs-haosu</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"><span style="color:red">Hao Su and Ravi cluster</span></td>
      <td markdown="span">NVME</td>
      <td markdown="span">131 TB</td>
    </tr>
    <tr>
      <td markdown="span">rook-cephfs-suncave</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"><span style="color:red">UCSD Suncave data only</span></td>
      <td markdown="span">SSD</td>
      <td markdown="span">8 TB</td>
    </tr>
    <tr>
      <td markdown="span">rook-ceph-block</td>
      <td markdown="span">(*default*) RBD</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteOnce</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">2.1 PB</td>
    </tr>
    <tr>
      <td markdown="span">rook-ceph-block-east</td>
      <td markdown="span">RBD</td>
      <td markdown="span">US East</td>
      <td markdown="span">ReadWriteOnce</td>
      <td markdown="span"></td>
      <td markdown="span">Mixed</td>
      <td markdown="span">500 TB</td>
    </tr>
    <tr>
      <td markdown="span">rook-cephfs-hawaii</td>
      <td markdown="span">CephFS</td>
      <td markdown="span">Hawaii+Asia (Coming soon)</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span"></td>
      <td markdown="span">Spinning drives with NVME meta</td>
      <td markdown="span">192TB</td>
    </tr>
  </tbody>
</table>

Ceph shared filesystem (**CephFS**) is the primary way of storing data in nautilus and allows mounting same volumes from multiple PODs in parallel (*ReadWriteMany*). Same applies to the **BeegFS** mounts accessed using NFS.

Ceph block storage allows [**RBD** (Rados Block Devices)](https://docs.ceph.com/docs/master/rbd/) to be attached to a **single pod** at a time (*ReadWriteOnce*). Provides fastest access to the data, and is preferred for smaller (below 500GB) datasets, and all datasets not needing shared access from multiple pods.

###### Creating and mounting the PVC

Use kubectl to create the PVC:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: examplevol
spec:
  storageClassName: <required storage class>
  accessModes:
  - <access mode, f.e. ReadWriteOnce >
  resources:
    requests:
      storage: <volume size, f.e. 20Gi>
```

After you've created a PVC, you can see it's status (`kubectl get pvc pvc_name`). Once it has the Status `Bound`, you can attach it to your pod (claimName should match the name you gave your PVC):

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: vol-pod
spec:
  containers:
  - name: vol-container
    image: ubuntu
    args: ["sleep", "36500000"]
    volumeMounts:
    - mountPath: /examplevol
      name: examplevol
  restartPolicy: Never
  volumes:
    - name: examplevol
      persistentVolumeClaim:
        claimName: examplevol
```

###### Using the right region for your pod
{: id="using-the-right-region-for-your-pod"}

Latency is significantly affecting the I/O performance. If you want optimal access speed to Ceph, add the region affinity to your pod for the correct `region` (`us-east` or `us-west`):

```yaml
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: topology.kubernetes.io/region
            operator: In
            values:
            - us-west
```

You can list the nodes region label using: `kubectl get nodes -L topology.kubernetes.io/region`


###### Volumes expanding
{: id="vol_expand"}

All ceph volumes created starting from December 2020 can be expanded by simply modifying the `storage` field of the PVC (either by using `kubectl edit pvc ...`, or `kubectl update -f updated_pvc_definition.yaml`)

For older ones, all `rook-ceph-block` and most `rook-cephfs`, `rook-cephfs-haosu` and `rook-cephfs-east` volumes can be expanded. If yours is not expanding, you can ask cluster admins to do it in manual mode.

###### Mounting pre-assigned folders (**deprecated**)

If you have a CephFS FOLDER assigned with a secret CEPH_KEY, to use it you first need to create a secret in your NAMESPACE:

```bash
kubectl create secret -n NAMESPACE generic ceph-fs-secret --from-literal=key=CEPH_KEY
```

Then use the secret in your pod volume (by default the folder name in path corresponds to your user name):
```yaml
 volumes:
 - name: fs-store
   flexVolume:
     driver: ceph.rook.io/rook
     fsType: ceph
     options:
       clusterNamespace: rook
       fsName: nautilusfs
       path: /FOLDER
       mountUser: USER
       mountSecret: ceph-fs-secret
```

Also add a volumeMounts section (see above) to mount the volume into your pod.

<p><a href="{{ page.url }}" class="sbutton">Back to top</a> </p>
