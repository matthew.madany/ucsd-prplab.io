---
layout: collection-userdocs
title: Syncthing
date: 2020-07-22
short: userdocs
categories: user
order: 50
---

[SyncThing](https://syncthing.net) is a tool to synchronise files collections between several devices with no single server, which creates a mesh between all devices and works well for large files collections. We currently have it deployed on Washington state node. You can request access to it by [contacting us](/userdocs/start/contact/).
