---
layout: collection-userdocs
title: Local
date: 2019-01-10
short: userdocs
categories: user
order: 7
---

Most nodes in the cluster have local NVME drives, which provide faster I/O than shared filesystems. These can be used for workloads that require very intensive I/O operations ([see recommendations and an example for running these](/userdocs/running/io-jobs/)).

You can request an [ephemeral volume](https://kubernetes.io/docs/concepts/storage/volumes/#emptydir) to be attached to your pod as a fast scratch space. Note that any information stored in it will be destroyed after pod shutdown.

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: myapp
spec:
  template:
    spec:
      containers:
      - name: demo
        image: gitlab-registry.nautilus.optiputer.net/prp/jupyterlab
        command:
        - "python"
        args:
        - "/home/my_script.py"
        - "--data=/mnt/data/..."
        volumeMounts:
        - name: data
          mountPath: /mnt/data
        resources:
          limits:
            memory: 8Gi
            cpu: "6"
            nvidia.com/gpu: "1"
            ephemeral-storage: 100Gi
          requests:
            memory: 4Gi
            cpu: "1"
            nvidia.com/gpu: "1"    
            ephemeral-storage: 100Gi
      volumes:
      - name: data
        emptyDir: {}
      restartPolicy: Never
  backoffLimit: 5
```
