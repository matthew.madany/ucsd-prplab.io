---
layout: collection-userdocs
title: Start with Storage
date: 2019-01-10
short: userdocs
categories: user
order: 0
---

This section describes the ways to access more than a **petabyte** of distributed Ceph storage, along with some other ways to store data available to users on Nautilus cluster.

Use menu on the left for topics in this section.<br>
Use tabs in the top navigation bar to switch to another section.
