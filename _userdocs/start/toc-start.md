---
layout: collection-userdocs
title: Where to start
date: 2019-01-10
short: userdocs
categories: user
order: 0
---

   * **Start**
     * [Get access](/userdocs/start/get-access): Getting authenticated in the cluster
     * [Quick start](/userdocs/start/quickstart): Basic info, links to external tutorials
     * [Policies](/userdocs/start/policies): **VERY IMPORTANT** section! Please read to avoid issues running in the cluster
     * [Available resources](/userdocs/start/resources): Additional resources you can get in the cluster
   * **Running**: running various kinds of jobs in the cluster efficiently
     * [Jupyter&Tensorflow2 example](/userdocs/running/jupyter): A comprehensive example on how to run the image we provide, containing most popular python libraries for ML
     * [Running batch jobs](/userdocs/running/jobs)
     * [Dealing with high I/O](/userdocs/running/io-jobs)
     * [Running permanent services](/userdocs/running/ingress)
     * [Idle pods](/userdocs/running/long-idle/): How to run "login" pods
     * [GPUs](/userdocs/running/gpu-pods): Requesting GPUs
     * [Client scripts](/userdocs/running/scripts): How to use a script from your laptop **and not get blocked**
     * [More computing resources](/userdocs/running/special/): How to get maximum from the cluster
   * **Storage**: accessing persistent storage
     * [Ceph storage](/userdocs/storage/ceph-posix/): How to get persistent filesystem attached to your pod
     * [Ceph S3](/userdocs/storage/ceph-s3/): Better scalable Object storage for billions of files
     * [Local](/userdocs/storage/local/): Attaching high-speed local scratch space
     * [Nextcloud](/userdocs/storage/nextcloud/): How to share your data and move data into the cluster
   * **Development**: efficient techniques to develop your own workflow
     * [GitLab](/userdocs/development/gitlab/): Getting access to unlimited space and CI/CD for your projects through our hosted GitLab
     * [Private repo](/userdocs/development/private-repos/): How to keep your project private and work with it in Nautilus

