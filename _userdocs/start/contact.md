---
layout: collection-userdocs
title: Contact
date: 2020-04-16
short: userdocs
categories: user
group: help
order: 70
---

## [matrix]

We're using the new innovative federated communication system [https://matrix.org](https://matrix.org) for all communications regarding the project. It's deployed in our cluster, and you can create an account in it using any compatible client.

#### Joining via web page

1. Navigate to [our hosted web version of element.io](https://element.nrp-nautilus.io)

2. Choose _Create Account_, then create an account by entering a desired username, password and (optional) email.

3. After that you'll get an account of the form `@username:matrix.nrp-nautilus.io`, which you can use to join rooms in any federated [matrix] resourse, including the original matrix.org.

4. Click explore rooms button. Check that you're exploring the NRP server (matrix.nrp-nautilus.io). Join the Nautilus General channel. Also join the News channel.

5. (Optional) Join the NRP Community by clicking the `+nrp:matrix.nrp-nautilus.io` URL in the header of the General channel. Another link to it is: <https://matrix-to.nrp-nautilus.io/#/+nrp:matrix.nrp-nautilus.io>. It has a list of channels you can join.

**Make sure to backup your encryption key to always have access to your encrypted messages! Go to User Settings -> Security & Privacy and click Start using Key Backup.** There's a pretty detailed [FAQ on using the Matrix Element client](https://element.io/help) including the security. Having email set will let you recover your account password (**but not the encryption keys!**)

#### Joining via apps

1. Get a [phone or desktop version of element](https://element.io/get-started) or [any other compatible client](https://matrix.org/clients/)

2. Change the server to the NRP one:

  * Click the change server button <img class="" src="/images/userdocs/matrix_server.png" width="400px">
  * The NRP matrix homeserver URL is `https://matrix.nrp-nautilus.io`

3. Follow the Joining via web page guide from step 2

#### Possible issues

If you've created the account on matrix.org server (not the NRP one), and have the user ID like `@username:matrix.org`, you can use the standard <https://app.element.io> web page to log in.

## RocketChat (deprecated)

Our hosted RocketChat instance is located at [https://rocket.nautilus.optiputer.net](https://rocket.nautilus.optiputer.net).
