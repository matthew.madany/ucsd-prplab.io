---
layout: collection-userdocs
title: Available Resources
date: 2019-01-10
short: userdocs
categories: user
order: 50
---

Although you can run your own containers, there are several services and resources 
already deployed by cluster admins that you can use without creating those yourself.

- [JupyterLab][1] 
- [EtherPad][2] 
- [Drone Images stitching: WebODM (Web Open Drone Map)][3]
- [Code and containers repository: GitLab][4]
- [File sharing: Nextcloud][5]
- [Network monitoring: traceroute tool][6]
- [File sharing: SyncThing][7] ([Contact us to set up](/userdocs/start/contact))

[1]: https://jupyterhub.nautilus.optiputer.net/
[2]: https://etherpad.nautilus.optiputer.net/
[3]: https://webodm.nautilus.optiputer.net/
[4]: https://gitlab.nautilus.optiputer.net/
[5]: https://nextcloud.nautilus.optiputer.net/
[6]: https://traceroute.nautilus.optiputer.net/
[7]: https://syncthing.net
