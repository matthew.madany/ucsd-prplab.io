---
layout: workshop
title: "La Jolla Kubernetes-Grafana  contact"
date: 2019-02-19
workshop: lajolla2019k8s
short: lajolla2019k8s
img: lajolla2019k8s/k8s.png
---

- [John Graham ][1]
- [Dima Mishin][2]
- [Nadya Williams][3]

[1]: mailto:jjgraham@eng.ucsd.edu
[2]: mailto:dmishin@ucsd.edu
[3]: mailto:nwilliams@ucsd.edu

