---
layout: workshop
title: "La Jolla Kubernetes-Grafana workshop materials"
date: 2019-02-19
workshop: lajolla2019k8s
short: lajolla2019k8s
img: lajolla2019k8s/k8s.png
---

<div class="font-weight-bold text-info" id="instructors">Prerequisites</div>
If you are interested in doing hands-on exercises during the work shop please 
do the following steps before you come to the workshop. 
These will allow you to setup needed tools on your personal laptop and use it for the hands-on exercises
during he workshop.  You can choose to skip hands-on and simply follow the demo part during the workshop.

1. Make sure you have a terminal window (Term, Xterm, etc) where you will be executing commands 
1. Make sure you have an editor to make simpel edits in text files (Vim, Emacs, Atom or similar)
1. Please  take a look at the documentation page [How to start using Nautilus cluster][3] 
   and follow instructions for the first 2 items shown on the left side menu of that page:

   - [Get Access][1]
      This is a simple step by step guide to become an authenticated  portal user. If you are already a  portal  user
      you can skip this part. If your institution supports CILogon, use your institutional identity, otherwise use your Google
      account.  
   - [Quick Start][2]
      Please verify steps 1-5 are working for you, skip the rest.

Please note, the exercises will not require extensive resources from your laptop.
The above instructions are for unix-based laptops (linux, mac). If you have windows-based laptop
please let us know (see [Contact][4]), there is a different set of instructions for Quick Start to accomplish the same.

[1]: /userdocs/start/get-access/
[2]: /userdocs/start/quickstart/
[3]: /userdocs/start/toc-start/
[4]: /lajolla2019k8s-contact/

[Back to top]({{page.url}})

