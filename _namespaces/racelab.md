---
layout: namespace
title: racelab
date: 2019-05-29
name: racelab
pi: Chandra Krintz and Rich Wolski
institution: University of California, Santa Barbara
software: Keras, TensorFlow, Kubernetes, Docker, OpenCV, RabbitMQ/Celery, Scikit-learn, NumPy
tagline: Image-recognizing Neural Network
imagesrc: racelab.png
categories: 
- "namespace" 
tags: []
---

In The UCSB RACE Lab we are developing a new Functions-as-a-Service (FaaS) capability for Nautilus.  The goal of the project 
is to be able to develop applications for Nautilus in the same way that they program commercial FaaS platforms such as AWS Lambda, 
Microsoft Functions, and Google Functions.  FaaS has emerged as a simple, highly scalable programming model that enables rapid development 
of scalable web services for deployment in public clouds.  However, to date, no FaaS platform (commercial or otherwise) allows the 
development of services that use GPU accelerators to perform their operations.  Our goal is provide a scalable FaaS capability for 
Nautilus that will enable programmers to write, debug, and deploy web services which use GPU processing to perform Machine Learning 
and AI computations in response to web requests.
<br>
Currently, we have a simple FaaS prototype working with Nautilus that we are using to train an image-recognizing Neural Network for 
doing automatic camera trap image classification.  Our intention is to incorporate the capability into the 
<a href="https://vimeo.com/253527900">Where's The Bear? project</a> and then to explore providing such capabilities to the ecological science community at large.
<br>
This work is primarily due to Michael Zhang and his advisors and Kate McCurdy who is director of the Sedgwick Science Reserve Michael 
is a doctoral candidate in the Computer Science Department at UCSB and a
member of the <a href="https://sites.cs.ucsb.edu/~ckrintz/racelab.html">RACE Lab</a>.
<br>
In addition, we are planning to use this new capability as part for the <a href="https://sites.cs.ucsb.edu/~ckrintz/projects/index.html">
UCSB SmartFarm Project</a>.
This project uses on-line, real-time machine learning algorithms and a new edge-computing targeted distributed platform called SPOT 
(Software Platform of Things) to manage Internet of Things applications and deployments.
