---
layout: namespace
title: wifire
date: 2019-05-22
name: wifire
pi: Ilkay Altintas
institution: University of California, San Diego
software: Kepler, Vert.x, FARSITE, GDAL
tagline: Modeling, data assimilation, and visualization of wildfires
imagesrc: wifire.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

<a href="https://wifire.ucsd.edu">WIFIRE</a> provides real-time curation and integration of public and private datasets 
related to wildfires and the environment. Their operational product, Firemap, has been successful delivering real-time 
predictions in initial attack for many fires, bringing together research and operational components in a unique fashion 
to deliver <a href="https://firemap.sdsc.edu">initial attack models of an ongoing fire</a> in a matter of minutes. City, 
County, and State fire agencies are using WIFIRE's Firemap for daily operations, and are closely collaborating with WIFIRE 
to expand its capabilities to fire agency needs.
