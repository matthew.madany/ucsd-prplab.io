---
layout: namespace
title: adalab
date: 2019-05-22
name: adalab
pi: Arun Kumar
institution: University of California, San Diego
software: Python, NumPy, CUDA, TensorFlow, PyTorch, CNTK, MXNet, Dask, Celery, Ray, Horovod
tagline: Advanced Data Analytics
imagesrc: adalab.png
categories: 
- "namespace" 
tags: [3D modeling]
---

<b>Project 1: Efficient and Reproducible Model Selection on Deep Learning Systems</b>
We are building a new parallel system to accelerate the model selection process of deep learning models, while ensuring 
reproducibility. Existing approaches either incur high resource costs (e.g., network overheads, memory/storage costs) or 
are not reproducible. Our system, Cerebro, alleviates these limitations by treating the model selection process as a 
multi-task optimization problem and offers the best efficiency while ensuring reproducibility.
<br><br>
<b>Project 2: Semi-Automatic ML Feature Type Inference</b>
We aim to semi-automate the task of inferring feature types in a dataset for ML models, an important and currently mostly 
manual step when applying to structured data. We compare and evaluate the suitability of different ML techniques, including 
deep learning models, for automating this task.
<br><br>
<b>Project 3: Predicting Eating Events for Initiating Interventions for Obese Individuals</b>
We are exploring the feasibility of predicting individuals' eating behavior from GIS and movement data using ML techniques, 
including deep learning models. The predictions will guide interventions (e.g., SMS messages, automated phone calls) to avoid 
obese individuals consuming unhealthy food.
