---
layout: namespace
title:  deepgtex-prp
date: 2018-12-10
name: deepgtex-prp
pi: F. Alex Feltus, Professor of Genetics & Biochemistry
institution: Clemson University
software: Python, Conda, TensorFlow, Scikit-learn, NumPy, Argparse, Matplotlib, Halo, Gene Oracle
tagline: Deep Learning in Oncogenomics
imagesrc: deepgtex-prp.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

The Feltus lab is running deep learning oncogenomics workflows on the Pacific Research Platform 
Kubernetes (K8s) cluster. The PRP K8s is allowing us to scale up our analyses by moving large 
genomics datasets between FIONA nodes and then screening tens of thousands of genes on GPUs 
for tumor biomarker discovery.
