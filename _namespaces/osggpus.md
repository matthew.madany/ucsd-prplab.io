---
layout: namespace
title: osggpus
date: 2019-03-22
name: osggpus
pi: Frank Wuerthwein
institution: University of California, San Diego
software: OpenGL
tagline: Photon propagation simulation
imagesrc: osggpus.png
categories: 
- "namespace" 
tags: [3D modeling]
---

<b>IceCube and GPUs:</b>
One of the common pieces of the OSG Software middleware stack is the HTCondor-CE. The HTCondor-CE 
is based entirely on the HTCondor software stack and provides an entrances for OSG pilots into 
the PRP's GPU cluster. These opportunistic GPU resources harvested by glideinWMS pilots and are 
used by IceCube. The ICeCube experiment uses the GPU computing power to simulate photon propagation.
