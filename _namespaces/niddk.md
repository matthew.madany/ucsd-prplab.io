---
layout: namespace
title:  niddk
date: 2019-01-14
name: niddk
pi: Loki Natarajan
institution: University of California, San Diego
software: TensorFlow, Python, NumPy, CUDA
tagline: Deep Learning for predicting human activity levels from raw accelerometer data
imagesrc: niddk.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

In this project, we develop new Deep Learning based techniques for predicting human activity levels (e.g., sitting, standing, and stepping) from raw tri-axial accelerometer data. The data is collected from a large cohort of human subjects who wore accelerometer devices for seven days of free living. Our goal is to develop accurate methods to predict human activity from these accelerometer data and then use them in downstream human activity and metabolic health correlation analysis.
<br><br>
The challenges of this project include working with large volumes of training data (~1 TB) and performing extensive model selection such as neural architecture search and hyperparameter tuning. We are actively using CHASE-CI's persistent storage to store our input and intermediate data. We parallelize the model selection using multiple on-demand GPU virtual machines.


