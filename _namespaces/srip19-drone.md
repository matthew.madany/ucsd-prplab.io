---
layout: namespace
title: srip19-drone
date: 2019-05-22
name: srip19-drone
pi: Nuno Vasconcelos
institution: University of California, San Diego
software: Python, CUDA, PyTorch, TensorFlow, Caffe, NumPy, OpenCV
tagline: Machine Learning and robotics
imagesrc: srip19-drone.png
categories: 
- "namespace" 
tags: []
---

Image collection with drones: The last few years have shown that a critical component in the design of effective image 
classification systems is the availability of large training datasets. Drones are a new way to collect large numbers of 
images of objects in a relatively inexpensive manner. We are interested in collecting datasets of objects under many views 
and in collecting datasets of scenes. The students will develop protocols for the use of drones in data collection and 
apply those protocols to the assembly of a few datasets. These will then be used to train deep learning systems for object 
recognition. The development will be for the Intel Aero drone, using the Robotic Operating System (ROS).
