---
layout: namespace
title: satellite-unicef
date: 2019-05-22
name: satellite-unicef
pi: Mai Nguyen
institution: University of California, San Diego
software: Caffe, Scikit-learn, Spark, GDAL
tagline: Machine learning to locate schools in rural Africa
imagesrc: satellite-unicef.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

This project addresses the problem of locating schools in rural regions of Liberia through the use of deep learning to 
analyze high-resolution satellite images.  Accurate data about schools and their infrastructure is needed to guide aid 
workers, policy makers, and philanthropic organizations in allocating essential resources in order to improve educational 
opportunities.  In this work, we use unsupervised learning methods with deep learning models to analyze satellite images 
at scale. Our approach provides a way to quickly filter out irrelevant data in a scene to identify regions of interest.  
Our results suggest that using machine learning with high resolution satellite images can reduce the search space to 
effectively filter out, help find schools with high recall, and aid appropriate and relevant resource allocations.
