---
layout: namespace
title: ncmir-mm
date: 2019-06-08
name: ncmir-mm
pi: Mark Ellisman
institution: University of California, San Diego
software: CDeep3M, TensorFlow, Caffe, Keras
tagline: Image segmentation with deep convolutional neural networks
imagesrc: ncmir-mm.png
categories: 
- "namespace" 
tags: []
---

Teravoxel scale image segmentation of 3D electron microscopy volumes with deep convolutional neural networks: 
We develop novel A.I. workflows for microanalysis and reconstruction of neural tissue and biological samples.
