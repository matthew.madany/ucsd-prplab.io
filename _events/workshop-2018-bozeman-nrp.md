---
layout: page
title: The 2nd NRP Workshop
date: 2018-08-06
dates: 2018 August 6-7
location: Bozeman, Montana
siteimage: events.png
categories:
- "workshop"
- "2018"
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/redWjg0OkQY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
For two years, the National Research Platform Workshop has brought together representatives from interested institutions to discuss implementation strategies for deployment of interoperable Science DMZs at a national, and potentially international, scale. The viewpoints of researchers, cyberinfrastructure experts, and CIOs, as well as perspectives from regional and national networks, help shape the program. Recent experiences from projects such as the National Science Foundation-funded Pacific Research Program, along with analogous national and international projects, are essential features of the workshop. 
The NRP Workshop has been co-sponsored by Calit2/PRP, Internet2, ESnet, Montana State University, and CENIC. It has support from the National Science Foundation.

#### Sunday, August 5, 2018: Intensive Introduction to Kubernetes Container Orchestration

1. Introduction to Containers (Shawfeng Dong). Demo: Installing JupyterHub container (Nadya Williams)
1. Instantiating a Single-Node Cluster and Setting Up Policies (Dmitry Mishin). Demo: Configure a local Kubernetes instance, run some containers in it; disk-to-disk transfer applications
1. Orchestrating Persistent, Distributed Cluster Storage (Dmitry Mishin). Demo: Rook orchestration of Ceph persistent, distributed storage; deploy perfSONAR tools
1. MPI Parallel-Distributed Computation on Kubernetes (Christopher Paolini). Demo: Numerical simulation of geologic CO2 and waste-water injection
1. Cluster Performance Monitoring and Visualization (Kishor Aher). Demo: Kubeflow deployment of machine learning (ML) workflows on Kubernetes; migrating from a laptop to a Google Kubernetes Engine (GKE) cluster

#### Monday, August 6, 2018: NRP Workshop Day One

1. Keynote: Rethinking NSF's Computational Ecosystem for 21st Century Science and Engineering (Manish Parashar)  
1. PRP, NRP, GRP, and the Path Forward (Larry Smarr)
1. Science Drivers for NRP (Alexander Szalay, Shawfeng Dong, Christopher Paolini, Dan Stanzione)
1. NRP Pilot Testbed (Jen Leasure, James Deaton, Thomas DeFanti, Louis Fox, Wendy Huntoon, Akbar Kara, John Moore, Bill Owens)       
1. Cloud Successes & Challenges: Public/Private (James Barr von Oehsen, Alex Feltus)      
1. NSF Experimental Clouds in the NRP: CloudLab, Chameleon, ExoGENI (Ilya Baldin, Kate Keahey, Joe Mambretti, K.C. Wang, John Moore)
1. Challenges and Opportunities of EPSCoR and Under-Resourced Universities (Wendy Huntoon, Damian Clark, Deborah Dent, Doug Jennewein, Matthew Ketterling, Jeff Letourneau)
1. Scaling Within a Campus: Case Study (Jerry Sheehan)
1. FIONA Workshops Series: Results & Next Steps (Hervey Allen, John Hess)

#### Tuesday, August 7, 2018: NRP Workshop Day Two

1. Keynote: A Modern Cyberinfrastructure: The Ladder to the Shoulders of Giants (Eli Dart)
1. EPSCoR & How To Train Many More Engagement Professionals (Loretta Moore)
1. Building the NRP Ecosystem: What Campuses Need To Support Them Being Involved in NRP (Jim Bottum, Dana Brunson, Loretta Moore, Josh Sonstroem, Dan Stanzione, Jeffrey Weekley)
1. Scaling Across the NRP Ecosystem from Campus to Regional to National: What Support Is There? (Jen Leasure, Lars Fischer, Marla Meehl, Inder Monga, Kate Petersen Mace, Howard Pfeffer, Jennifer Schopf)
1. International-Scale Measurement Technologies/Techniques (Kate Petersen Mace, Lars Fischer, Dima Mishin, Jeonghoon Moon, Jennifer Schopf)
1. Data Science & Applications in Exascale World (Venkatram Vishwanath)   
1. Scheduling Computational & Storage Resources on the NRP (Rob Gardner)
1. Open Science Grid (OSG) & Internet2: Working Together on the NRP (John Moore, Frank Wuerthwein)
1. NRP and Near-Term NSF Funding Opportunities, Discussion & Closing Remarks (Ana Hunsinger, Inder Monga, Manish Parashar, Larry Smarr)

