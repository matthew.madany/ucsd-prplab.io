---
layout: page
title: Advanced AI Workflows
date: 2018-09-11
dates: 2018 September 11
location: Santa Clara, CA
siteimage: micro.png
categories:
- "workshop"
- "2018"
---

####  Applying Advanced AI Workflows in Astronomy and Microscopy 

This one-day workshop focused on the challenges shared between observational astronomy and modern microscopy workflows. Advances in instrumentation, computation and data management for both microscopy and astronomy suggest opportunities for the broader scientific community to learn from the challenges common to both fields. The high-velocity, high-volume data generated by endeavors such as the Large Synoptic Survey Telescope (LSST) or the Square Kilometer Array (SKA) will require new techniques and cyberinfrastructure such as that provided by the Pacific Research Platform to handle the data they will produce. Best practices developed in these contexts may be applied to other domains.

We introduced some of the challenges in microscopy and provided a survey of the techniques available to astronomers to deal with the ever-increasing flow of data, such as adopting streaming workflows (versus file-based workflows), Artificial Intelligence and Machine Learning, and GPU-accelerated analysis, as well as storage and data distribution, approaches that scale to the needs of a global community of researchers engaged in advanced microscopy imaging.

<a href="https://ucsd-prp.gitlab.io/media/2018_summit.html" class="button" data-fancybox data-options='{"type" : "iframe", "iframe" : {"preload" : false, "css" : {"width" : "600px"}}}'> Slides </a>
<a href="https://www.eventbrite.com/e/workshop-on-applying-advanced-ai-workflows-in-astronomy-and-microscopy-tickets-47324338370#" class="button" data-fancybox data-options='{"type" : "iframe", "iframe" : {"preload" : false, "css" : {"width" : "1000px"}}}'> Eventbrite Infomation Page </a>  
