--- 
layout: page
title: NORDUnet-SURFnet FIONA Workshop
date: 2018-04-16
dates: 2018 April 16-17
location: Kastrup, Denmark
siteimage: events.png
categories:
- "workshop"
- "2018"
---

NORDUnet - SURFnet FIONA workshop 
April 16-17 2018 at the NORDUnet HQs in Kastrup, Denmark

