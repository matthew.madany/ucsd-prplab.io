---
layout: page
title: DIBBs18 Workshop
date: 2018-07-09
dates: 2018 July 9-10
location: Arlington, Virginia
siteimage: dibbs18.png
categories:
- "workshop"
- "2018"
---

[The Second NSF PI Data Infrastructure Building Blocks workshop July 9 - 10, 2018, Arlington, Virgina][dibbs18]

The National Science Foundation Cyberinfrastructure Framework for 21st Century Science and Engineering (CIF21) considers an integrated, scalable, and sustainable cyberinfrastructure to be crucial for the advancement of new research practices and transformative advances across all fields of science and engineering. The Data Infrastructure Building Blocks (DIBBs) program supported that vision by encouraging the development of robust and shared data-centric cyberinfrastructure capabilities.

Following on to, and closely adapting the rationale and structure of the first DIBBs17 PI Workshop developed by Dr. David Lifka and Paul Redfern of Cornell University, a second NSF Data Infrastructure Building Blocks PI Workshop (DIBBs18) was convened in Arlington, VA during July 9-10, 2018 to exchange results and lessons learned from the projects, and to consider the implications of project results for advances in the vision and goals for data cyberinfrastructure, with the focus on Harnessing the Data Revolution, one of the NSF Big Ideas.

<a href="https://dibbs18.ucsd.edu/" class="button">DIBBs18 Website</a>


[dibbs18]: https://dibbs18.ucsd.edu/

